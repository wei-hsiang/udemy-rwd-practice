import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Session10HomworkComponent } from './session10-homwork.component';

describe('Session10HomworkComponent', () => {
  let component: Session10HomworkComponent;
  let fixture: ComponentFixture<Session10HomworkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Session10HomworkComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Session10HomworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
