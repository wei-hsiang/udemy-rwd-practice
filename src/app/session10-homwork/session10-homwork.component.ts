import { Component } from '@angular/core';
import { faCodeFork,faSpoon  } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-session10-homwork',
  templateUrl: './session10-homwork.component.html',
  styleUrls: ['./session10-homwork.component.scss']
})

export class Session10HomworkComponent {
  faCodeFork = faCodeFork;
  faSpoon = faSpoon;
}
