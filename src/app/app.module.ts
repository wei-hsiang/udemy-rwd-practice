import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Css3MediaQueryComponent } from './css3-media-query/css3-media-query.component';
import { RouterModule, Routes} from '@angular/router';
import { Session2HomworkComponent } from './session2-homwork/session2-homwork.component';
import { Session10HomworkComponent } from './session10-homwork/session10-homwork.component';
import { LayoutModule } from './layout/layout.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Session10HomeworkCartComponent } from './session10-homework-cart/session10-homework-cart.component';

const appRoutes: Routes = [
  {path: 'css3-media-query', component: Css3MediaQueryComponent},
  {path: 'session2-homework', component: Session2HomworkComponent},
  {path: 'session10-homework', component: Session10HomworkComponent},
  {path: 'session10-homework-cart', component: Session10HomeworkCartComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    Css3MediaQueryComponent,
    Session2HomworkComponent,
    Session10HomworkComponent,
    Session10HomeworkCartComponent,
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    FontAwesomeModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
