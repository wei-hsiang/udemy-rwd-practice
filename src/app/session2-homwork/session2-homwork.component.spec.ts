import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Session2HomworkComponent } from './session2-homwork.component';

describe('Session2HomworkComponent', () => {
  let component: Session2HomworkComponent;
  let fixture: ComponentFixture<Session2HomworkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Session2HomworkComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Session2HomworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
