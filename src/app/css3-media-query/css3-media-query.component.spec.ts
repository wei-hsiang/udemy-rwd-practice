import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Css3MediaQueryComponent } from './css3-media-query.component';

describe('Css3MediaQueryComponent', () => {
  let component: Css3MediaQueryComponent;
  let fixture: ComponentFixture<Css3MediaQueryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Css3MediaQueryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Css3MediaQueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
