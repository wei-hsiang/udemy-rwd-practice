import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Session10HomeworkCartComponent } from './session10-homework-cart.component';

describe('Session10HomeworkCartComponent', () => {
  let component: Session10HomeworkCartComponent;
  let fixture: ComponentFixture<Session10HomeworkCartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Session10HomeworkCartComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Session10HomeworkCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
