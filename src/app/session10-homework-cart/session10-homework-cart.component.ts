import { Component } from '@angular/core';
import { faLeaf,faSearch,faCartPlus } from '@fortawesome/free-solid-svg-icons';
import { faHeart,faSmileBeam } from '@fortawesome/free-regular-svg-icons';
@Component({
  selector: 'app-session10-homework-cart',
  templateUrl: './session10-homework-cart.component.html',
  styleUrls: ['./session10-homework-cart.component.css']
})
export class Session10HomeworkCartComponent {
    faLeaf = faLeaf;
    faSearch = faSearch;
    faCartPlus = faCartPlus;
    faHeart = faHeart;
    faSmileBeam = faSmileBeam;
}
