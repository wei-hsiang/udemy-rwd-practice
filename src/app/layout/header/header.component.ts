import { Component } from '@angular/core';
import { faCodeFork,faSpoon  } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
})

export class HeaderComponent {
    navbarCollapsed = true;
    dropdownOpen = false;
    faCodeFork = faCodeFork;
    faSpoon = faSpoon;
}