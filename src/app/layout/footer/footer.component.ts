import { Component } from '@angular/core';
import { faSquareGooglePlus, faSquareTwitter, faSquareFacebook } from '@fortawesome/free-brands-svg-icons'

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent {
  faSquareGooglePlus = faSquareGooglePlus;
  faSquareTwitter = faSquareTwitter;
  faSquareFacebook = faSquareFacebook;
}
